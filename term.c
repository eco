/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>

#include "term.h"
#include "config.h"


static struct termios otermios; /* original terminal characteristics */
static struct termios ntermios; /* characteristics to use inside */

E_Term *e_term_open(void)
{
	struct winsize ws;
	E_Term *tr;
	char *s;

	tr= (E_Term *)malloc(sizeof(E_Term));
	if (!tr) {
		perror("malloc");
		exit(-1);
	}

	tr->name= getenv("TERM");
	if (!tr->name) {
		printf("Shell variable TERM not defined!");
		exit(-1);
	}

	if (strcmp(tr->name, "vt100") && strcmp(tr->name, "linux") &&
	    strcmp (tr->name, "xterm") && strcmp(tr->name, "ansi") &&
	    strcmp (tr->name, "xterm-256color")) {
		printf("Invalid terminal type: [%s]", tr->name);
		exit(1);
	}


	tcgetattr(0, &otermios);

	ntermios= otermios;
	ntermios.c_iflag&= ~(IGNBRK | BRKINT | IGNPAR | PARMRK
				| INPCK | INLCR | IGNCR | ICRNL | IXON);

	ntermios.c_oflag&= ~(OPOST | ONLCR | OLCUC | OCRNL | ONOCR | ONLRET);

	ntermios.c_lflag&= ~(ISIG | ICANON | XCASE | ECHO | ECHOE | ECHOK
				| ECHONL | NOFLSH | TOSTOP | ECHOCTL |
				ECHOPRT | ECHOKE | FLUSHO | PENDIN | IEXTEN);

	ntermios.c_cc[VMIN] = 1;
	ntermios.c_cc[VTIME] = 0;
	tcsetattr(0, TCSADRAIN, &ntermios);

	/* init default entrys. */
	tr->col= 999;
	tr->row= 999;
	tr->fgcolor= E_TR_WHITE;
	tr->bgcolor= E_TR_BLACK;

	if (ioctl(0, TIOCGWINSZ, &ws)) {
		s= getenv("LINES");
		if (!s)
			tr->nrow= 23;
		else {
			tr->nrow= atoi(s);
			tr->nrow--;
		}

		s= getenv("COLUMNS");
		if (!s)
			tr->ncol= 80;
		else
			tr->ncol= atoi(s);
	}
	else {
		tr->nrow= ws.ws_row-1;
		tr->ncol= ws.ws_col;
	}

	return(tr);
}

void e_term_close(E_Term *tr)
{
	e_term_fgcol(tr, E_TR_WHITE);
	e_term_bgcol(tr, E_TR_BLACK);
	e_term_eeop(tr);
	tcsetattr(0, TCSADRAIN, &otermios);
}

void e_term_putc(int c)
{
	fputc(c, stdout);
}

void e_term_flush(void)
{
	int status;

	status= fflush(stdout);
	while (status < 0 && errno == EAGAIN) {
		sleep(1);
		status= fflush(stdout);
	}
	if (status < 0)
		exit(15);
}

int e_term_getc(void)
{
	return(255 & fgetc(stdin));
}

void e_term_parm(int n)
{
	int q, r;

	q= n / 10;
	if (q != 0) {
		r= q / 10;
		if (r != 0) {
			e_term_putc((r % 10) + '0');
		}
		e_term_putc((q % 10) + '0');
	}
	e_term_putc((n % 10) + '0');
}

void e_term_fgcol(E_Term *tr, int color)
{
	if (color != tr->fgcolor) {
		e_term_putc(E_TR_ESC);
		e_term_putc('[');
		e_term_parm(color + 30);
		e_term_putc('m');
		tr->fgcolor= color;
	}
}

void e_term_bgcol(E_Term *tr, int color)
{
	if (color != tr->bgcolor) {
		e_term_putc(E_TR_ESC);
		e_term_putc('[');
		e_term_parm(color + 40);
		e_term_putc('m');
		tr->bgcolor= color;
	}
}

void e_term_move(int row, int col)
{
	e_term_putc(E_TR_ESC);
	e_term_putc('[');
	e_term_parm(row + 1);
	e_term_putc(';');
	e_term_parm(col + 1);
	e_term_putc('H');
}

void e_term_eeol(void)
{
	e_term_putc(E_TR_ESC);
	e_term_putc('[');
	e_term_putc('K');
}

void e_term_eeop(E_Term *tr)
{
	e_term_fgcol(tr, tr->fgcolor);
	e_term_bgcol(tr, tr->bgcolor);
	e_term_putc(E_TR_ESC);
	e_term_putc('[');
	e_term_putc('J');
}

void e_term_rev(E_Term *tr, int state)
{
	int ftmp, btmp;

	e_term_putc(E_TR_ESC);
	e_term_putc('[');
	e_term_putc(state ? '7' : '0');
	e_term_putc('m');

	if (!state) {
		ftmp = tr->fgcolor;
		btmp = tr->bgcolor;
		tr->fgcolor = -1;
		tr->bgcolor = -1;
		e_term_fgcol(tr, ftmp);
		e_term_bgcol(tr, btmp);
	}
}

void e_term_beep ( void )
{
	e_term_putc(E_TR_BEL);
	e_term_flush();
}
