/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_UPDATE_H
#define _ECO_UPDATE_H

void e_update(E_Eco *ec);

#endif /* _ECO_UPDATE_H */
