/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "config.h"


/* sections list. */
E_Section *sections= NULL;

int e_config_read_line(FILE *fp, char *buf, int len)
{
	int i, c;

	for (i= 0; i < len; i++) {
		c= fgetc(fp);
		if (c == EOF) {
			if (i) {
				buf[i]= '\0';
				return(i);
			}
			return(-1);
		}

		if (c == '\n') {
			buf[i]= '\0';
			return(i);
		}
		buf[i]= c;
	}

	/* the line is too big, so we skip this. */
	return(0);
}

int __e_config_is_section(char *line)
{
	char *p;

	p= line;
	while (p && (*p == ' ' || *p == '\t'))
		p++;

	if (*p == '[')
		return(0);
	return(1);
}

int __e_config_is_option(char *line)
{
	char *p;

	p= strchr(line, '=');
	if (p)
		return(0);
	return(1);
}

E_Section *__e_config_section(char *line)
{
	E_Section *sc;
	char *p;
	int i, found;

	sc= (E_Section *)malloc(sizeof(E_Section));
	sc->next= NULL;
	sc->list= NULL;
	sc->last= NULL;

	/* is valid if the line have space or tab befor the '[' */
	p= line;
	while (*p == ' ' || *p == '\t')
		p++;

	/* ok, this is the first character != of ' ' or '\t'
	 * and is the start of the section, '[' that is why
	 * we add a + two character.
	 */
	p+= 2;

	/* at this point we are in the start of the section name,
	 * but need take care that the section name maybe have space,
	 * for eg. [ Keys ] and not [Keys].. so we support both model.
	 */
	while (p && (*p == ' ' || *p == '\t'))
		p++;

	if (!p) {
		free((void *)sc);
		return(NULL);
	}

	/* count the number of character. */
	for (i= 0, found= 0; i < strlen(p); i++) {
		if (p[i] == ' ' || p[i] == ']') {
			found= 1;
			break;
		}
	}

	if (!found) {
		free((void *)sc);
		return(NULL);
	}

	sc->name= (char *)malloc(i+1);
	i= 0;
	while (p) {
		if (*p == ' ' || *p == ']') {
			sc->name[i]= '\0';
			break;
		}
		sc->name[i]= *p;
		i++;
		p++;
	}

	return(sc);
}

E_Config *__e_config_option(char *line)
{
	E_Config *cf;
	char *p;
	int i;

	cf= (E_Config *)malloc(sizeof(E_Config));
	cf->next= NULL;
	cf->key= NULL;
	cf->value= NULL;

	/* the options always can have tab or ' ' */
	p= line;
	while (*p == ' ' || *p == '\t')
		p++;

	/* count the number of characters. */
	for (i= 0; i < strlen(p); i++) {
		if (p[i] == ' ' || p[i] == '=')
			break;
	}

	cf->key= (char *)malloc(i+1);
	i= 0;
	while (p) {
		if (*p == ' ' || *p == '=') {
			cf->key[i]= '\0';
			break;
		}
		cf->key[i]= *p;
		i++;
		p++;
	}

	/* now the value. */
	p= strchr(line, '=');
	p++;

	/* the value also have space or tab, so skip it. */
	while (p && (*p == ' ' || *p == '\t'))
		p++;

	cf->value= (char *)malloc(strlen(p)+1);
	strcpy(cf->value, p);
	return(cf);
}

void e_config_parse(char *file)
{
	E_Section *sc;
	E_Config *cf;
	FILE *fp;
	char *line;
	int i;

	fp= fopen(file, "r");
	if (!fp)
		return;

	line= (char *)malloc(1024);
	sc= NULL;
	i= e_config_read_line(fp, line, 1023);
	while (i != -1) {
		if (i == 0)
			goto next_line;

		/* skip comments and blank lines */
		if (line[0] == '#' || line[0] == '\n')
			goto next_line;

		if (__e_config_is_section(line) == 0) {
			sc= __e_config_section(line);
			sc->next= sections;
			sections= sc;
			goto next_line;
		}

		/* if we don't have a section, skip this line. */
		if (!sc)
			goto next_line;

		if (__e_config_is_option(line) == 0) {
			cf= __e_config_option(line);
			cf->next= NULL;
			if (!sc->list)
				sc->list= cf;
			if (sc->last)
				sc->last->next= cf;
			sc->last= cf;
		}
next_line:
		i= e_config_read_line(fp, line, 1023);
	}

#ifdef WITH_DEBUG
	e_debug_printf("==> Config File <==\n");
	sc= sections;
	while (sc) {
		e_debug_printf("Section: (%s)\n", sc->name);
		cf= sc->list;
		while (cf) {
			e_debug_printf("\tKey: (%s)\n", cf->key);
			e_debug_printf("\tValue: (%s)\n", cf->value);
			cf= cf->next;
		}
		sc= sc->next;
	}
	e_debug_printf("==> End Config File <==\n");
#endif

	fclose(fp);
	free((void *)line);
}

void e_config_init(void)
{
	char *home;
	char *file;

	home= getenv("HOME");
	if (!home)
		return;

	file= (char *)malloc(strlen(home) + 6);
	sprintf(file, "%s/.eco", home);
	e_config_parse(file);
	free((void *)file);
}

E_Section *e_config_find_section(char *name)
{
	E_Section *p;

	p= sections;
	while (p) {
		if (!strcmp(p->name, name))
			return(p);
		p= p->next;
	}
	return(NULL);
}

E_Config *e_config_find_config(E_Section *sc, char *key)
{
	E_Config *p;

	p= sc->list;
	while (p) {
		if (!strcmp(p->key, key))
			return(p);
		p= p->next;
	}
	return(NULL);
}

char *e_config_get(char *name, char *key)
{
	E_Section *sc;
	E_Config *cf;

	sc= e_config_find_section(name);
	if (sc) {
		cf= e_config_find_config(sc, key);
		if (cf)
			return(cf->value);
	}
	return(NULL);
}

int e_config_get_int(char *name, char *key)
{
	E_Section *sc;
	E_Config *cf;

	sc= e_config_find_section(name);
	if (sc) {
		cf= e_config_find_config(sc, key);
		if (cf)
			return(atoi(cf->value));
	}
	return(-1);
}

char e_config_get_char(char *name, char *key)
{
	E_Section *sc;
	E_Config *cf;

	sc= e_config_find_section(name);
	if (sc) {
		cf= e_config_find_config(sc, key);
		if (cf)
			return((char)atoi(cf->value));
	}
	return((char)-1);
}
