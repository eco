/*
 * Copyright (C) 2010 Diego Hernan Borghetti.
 */

#ifndef _ECO_SYNTAX_H
#define _ECO_SYNTAX_H

typedef struct _E_Syntax_Word {
	struct _E_Syntax_Word *next;
	struct _E_Syntax_Word *prev;

	/* the character's for this word (or only one). */
	char *word;

	/* number of characters. */
	int nch;

	/* color to apply, foreground and background. */
	char fgcol;
	char bgcol;
} E_Syntax_Word;

typedef struct _E_Syntax_Char {
	struct _E_Syntax_Char *next;
	struct _E_Syntax_Char *prev;

	/* the character. */
	char ch;

	/* color to apply, foreground and background. */
	char fgcol;
	char bgcol;
} E_Syntax_Char;

typedef struct _E_Syntax {
	struct _E_Syntax *next;
	struct _E_Syntax *prev;

	/* language name. */
	char *name;

	/* Function to check the file type.
	 * The argument is the file extension or NULL!
	 * return zero on success or different that zero otherwise.
	 */
	int (*check_type)(char *);

	/* the dictionary of word's. */
	E_List dirs;

	/* and only character, for second round. */
	E_List chars;
} E_Syntax;

E_Syntax *e_syntax_search(char *ext);
void e_syntax_add(E_Syntax *sy);
void e_syntax_rem(E_Syntax *sy);
E_Syntax *e_syntax_new(void);
void e_syntax_free(E_Syntax *sy);
void e_syntax_word_add(E_Syntax *sy, char *word, char fg, char bg);
void e_syntax_char_add(E_Syntax *sy, char ch, char fg, char bg);
void e_syntax_apply(E_Syntax *sy, E_ScreenLine *ln, int ncol);

#endif /* _ECO_SYNTAX_H */
