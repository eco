/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_CONFIG_H
#define _ECO_CONFIG_H

typedef struct _E_Config {
	struct _E_Config *next;

	/* key, name ... */
	char *key;

	/* value, function, name ... */
	char *value;
} E_Config;

typedef struct _E_Section {
	struct _E_Section *next;

	/* section name. */
	char *name;

	/* list of key/value pair. */
	E_Config *list;
	E_Config *last;
} E_Section;

void e_config_init(void);
void e_config_parse(char *file);

E_Section *e_config_find_section(char *name);
E_Config *e_config_find_config(E_Section *sc, char *key);

char *e_config_get(char *name, char *key);
char e_config_get_char(char *name, char *key);
int e_config_get_int(char *name, char *key);

#endif /* _ECO_CONFIG_H */
