/*
 * Copyright (C) 2008 - 2012 Diego Hernan Borghetti.
 * Eco
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>

#include "debug.h"
#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "status.h"
#include "file.h"


/* Buffer size for read file. */
#define BUF_SIZE 8192

E_File_Path *e_file_get_paths(char *file)
{
	E_File_Path *paths;
	char *s, *pwd, *slast;
	int ndir_rem, i, count, len;

	paths= (E_File_Path *)malloc(sizeof(E_File_Path));
	paths->path= NULL;
	paths->file= NULL;
	paths->lock_file= NULL;
	paths->locked= 0;

	if (file[0] == '/') {
		/* this is a absolute path, nothing to do here,
		 * just get the path.
		 */
		s= strrchr(file, '/');
		paths->path= (char *)malloc(s - file + 1);
		strncpy(paths->path, file, s - file);
		paths->path[s - file]= '\0';
		paths->file= strdup(file);
	}
	else if (!strncmp(file, "..", 2)) {
		/* ok, the "hard" case, relative path. */
		ndir_rem= 1;
		slast= NULL;
		s= strstr(file, "..");
		s++; /* skip the first. */

		s= strstr(s, "..");
		while (s) {
			ndir_rem++;
			s++;
			slast= s;
			s= strstr(s, "..");
		}

		/* get the current directory. */
		pwd= getenv("PWD");

		/* now we need remove ndir_rem from the path. */
		len= strlen(pwd);
		count= 0;
		for (i= len-1; i > 0; i--) {
			if (pwd[i] == '/') {
				count++;
				if (count == ndir_rem)
					break;
			}
		}

		if (!slast)
			slast= file+3;
		else
			slast += 2; /* skip ./ */

		paths->path= (char *)malloc(i+1);
		strncpy(paths->path, pwd, i);
		paths->path[i]= '\0';
		paths->file= (char *)malloc(i + strlen(slast) + 2);
		strncpy(paths->file, pwd, i);
		sprintf(paths->file+i, "/%s", slast);
	}
	else {
		pwd= getenv("PWD");
		paths->path= strdup(pwd);
		paths->file= (char *)malloc(strlen(paths->path) + strlen(file) + 2);
		sprintf(paths->file, "%s/%s", paths->path, file);
	}

	if (paths->file) {
		paths->lock_file= (char *)malloc(strlen(paths->file)+6);
		sprintf(paths->lock_file, "%s.eco~", paths->file);
	}

	return(paths);
}

int e_file_is_lock(E_File_Path *paths)
{
	struct stat st;

	if (stat(paths->lock_file, &st))
		return(0); /* not locked. */
	return(1); /* locked. */
}

void e_file_lock(E_File_Path *paths)
{
	FILE *fp;

	fp= fopen(paths->lock_file, "w");
	if (fp) {
		paths->locked= 1;
		fprintf(fp, "Eco lock file\n");
		fclose(fp);
	}
}

void e_file_lock_rem(E_File_Path *paths)
{
	if (paths->locked) {
		unlink(paths->lock_file);
		paths->locked= 0;
	}
}

E_Buffer *e_file_read(char *file, int *status)
{
	struct stat st;
	E_Buffer *bf;
	FILE *fp;
	char buf[BUF_SIZE];
	int rb;

	/* clean error status. */
	*status= 0;

	bf= e_buffer_new(file);
	if (!bf)
		return(NULL);

	if (stat(bf->paths->file, &st)) {
		e_buffer_free(bf);
		return(NULL);
	}

	if (e_file_is_lock(bf->paths)) {
		/* we already have this file open!! in other session!! */
		*status= 2;
		e_buffer_free(bf);
		return(NULL);
	}

	if (!(S_ISREG(st.st_mode))) {
		/* this is not a regular file. */
		*status= 1;

		e_buffer_free(bf);
		return(NULL);
	}

	fp= fopen(bf->paths->file, "r");
	if (!fp) {
		/* the file exist, but we can't read it. */
		*status= 1;

		e_buffer_free(bf);
		return(NULL);
	}

	/* Mark the buffer to avoid auto-complet of
	 * new lines.
	 */
	bf->flag |= BUFFER_NOIDENT;

	/* Read the file. */
	do {
		rb= fread((void *)&buf[0], 1, BUF_SIZE, fp);
		if (rb > 0)
			e_buffer_insert_str(bf, &buf[0], rb);
	} while (rb == BUF_SIZE);

	/* always go to the start with new files. */
	e_buffer_goto_begin(bf);

	/* e_buffer_insert/newline mark the buffer with changes,
	 * but in this case we are reading the file, so unmark
	 * when we finish.
	 */
	BUFFER_UNSET(bf, BUFFER_FLUSH);

	/* and lock the file. */
	e_file_lock(bf->paths);

	/* Remove the mark. */
	bf->flag &= ~BUFFER_NOIDENT;

	fclose(fp);
	return(bf);
}

void e_file_write(E_Eco *ec, E_Buffer *bf)
{
	FILE *fp;
	E_Line *ln;
	int i, nlines;

	if (!(bf->flag & BUFFER_FLUSH))
		return;

	fp= fopen(bf->paths->file, "w");
	if (!fp)
		return;

	ln= bf->lines;
	nlines= 0;
	while (ln) {
		for (i= 0; i < ln->used; i++)
			fputc(ln->text[i], fp);

		if (ln->next)
			fputc('\n', fp);
		nlines++;
		ln= ln->next;
	}

	fclose(fp);
	BUFFER_UNSET(bf, BUFFER_FLUSH);

	e_status_set_msg(ec, "(Wrote %d lines)", nlines);
}
