/*
 * Copyright (C) 2010 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "debug.h"
#include "list.h"
#include "term.h"
#include "screen.h"
#include "syntax.h"


/* our syntax data base. */
static E_List syntaxs= { NULL, NULL };

static E_Syntax_Word *e_syntax_word_search(E_Syntax *sy, char *word, int nch)
{
	E_Syntax_Word *p;

	p= (E_Syntax_Word *)sy->dirs.first;
	while (p) {
		if (p->nch == nch) {
			if (!strncmp(p->word, word, nch))
				return(p);
		}
		p= p->next;
	}

	return(NULL);
}

static E_Syntax_Char *e_syntax_char_search(E_Syntax *sy, char ch)
{
	E_Syntax_Char *p;

	p= (E_Syntax_Char *)sy->chars.first;
	while (p) {
		if (p->ch == ch)
			return(p);
		p= p->next;
	}
	return(NULL);
}

E_Syntax *e_syntax_search(char *ext)
{
	E_Syntax *p;

	p= (E_Syntax *)syntaxs.first;
	while (p) {
		if ((*p->check_type)(ext) == 0)
			return(p);
		p= p->next;
	}
	return(NULL);
}

void e_syntax_add(E_Syntax *sy)
{
	if (sy) {
		E_LIST_ADD(&syntaxs, sy);
	}
}

void e_syntax_rem(E_Syntax *sy)
{
	if (sy) {
		E_LIST_REM(&syntaxs, sy);
	}
}

E_Syntax *e_syntax_new(void)
{
	E_Syntax *sy;

	sy= (E_Syntax *)malloc(sizeof(E_Syntax));
	sy->next= NULL;
	sy->prev= NULL;
	sy->name= NULL;
	sy->check_type= NULL;
	sy->dirs.first= NULL;
	sy->dirs.last= NULL;
	sy->chars.first= NULL;
	sy->chars.last= NULL;
	return(sy);
}

void e_syntax_free(E_Syntax *sy)
{
	E_Syntax_Word *p;

	while (sy->dirs.first) {
		p= (E_Syntax_Word *)sy->dirs.first;
		sy->dirs.first= sy->dirs.first->next;
		free((void *)p->word);
		free((void *)p);
	}

	if (sy->name)
		free((void *)sy->name);

	free((void *)sy);
}

void e_syntax_word_add(E_Syntax *sy, char *word, char fg, char bg)
{
	E_Syntax_Word *wr;

	wr= e_syntax_word_search(sy, word, strlen(word));
	if (wr)
		return;

	wr= (E_Syntax_Word *)malloc(sizeof(E_Syntax_Word));
	wr->next= NULL;
	wr->prev= NULL;
	wr->word= strdup(word);
	wr->nch= strlen(wr->word);
	wr->fgcol= fg;
	wr->bgcol= bg;
	E_LIST_ADD(&sy->dirs, wr);
}

void e_syntax_char_add(E_Syntax *sy, char ch, char fg, char bg)
{
	E_Syntax_Char *cha;

	cha= e_syntax_char_search(sy, ch);
	if (cha)
		return;

	cha= (E_Syntax_Char *)malloc(sizeof(E_Syntax_Char));
	cha->next= NULL;
	cha->prev= NULL;
	cha->ch= ch;
	cha->fgcol= fg;
	cha->bgcol= bg;
	E_LIST_ADD(&sy->chars, cha);
}

static inline int check_token_start(int nch)
{
	return(isalpha(nch));
}

static inline int check_token_end(int nch)
{
	return(!isalpha(nch));
}

static int e_syntax_word(char *line, int idx, int *istart, int nch)
{
	int i, tot, first;

	first= 0;

	/* Search the start of the word. */
	for (i= idx; i < nch; i++) {
		if (check_token_start(line[i])) {
			first= 1;
			break;
		}
	}

	if (!first)
		return(-1);

	*istart= i;
	tot= 0;

	e_debug_printf("Syntax word [");
	for (i= *istart; i < nch; i++) {
		if (check_token_end(line[i]))
			break;
		tot++;
	}
	if (tot)
		return(tot);
	return(-1);
}

static void e_syntax_color(E_Syntax_Word *wr, E_ScreenLine *ln, int i, int nch)
{
	int e;

	for (e= i; e < (i+nch); e++) {
		ln->fcol[e]= wr->fgcol;
		ln->bcol[e]= wr->bgcol;
	}
}

void e_syntax_apply(E_Syntax *sy, E_ScreenLine *ln, int ncol)
{
	E_Syntax_Word *wr;
	E_Syntax_Char *ch;
	int i, nch, istart;

	i= 0;

	/* First round, check for words. */
do_word:
	nch= e_syntax_word(ln->text, i, &istart, ncol);
	if (nch == -1) {
		goto do_char;
		return;
	}

	wr= e_syntax_word_search(sy, &(ln->text[istart]), nch);
	if (wr)
		e_syntax_color(wr, ln, istart, nch);

	if (i+nch < ncol) {
		i += nch;
		goto do_word;
	}

	/* Second round, check for characters. */
do_char:
	for (i= 0; i < ncol; i++) {
		ch= e_syntax_char_search(sy, ln->text[i]);
		if (ch) {
			ln->fcol[i]= ch->fgcol;
			ln->bcol[i]= ch->bgcol;
		}
	}
}
