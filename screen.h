/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_SCREEN_H
#define _ECO_SCREEN_H

typedef struct _E_ScreenLine {
	char *fcol;
	char *bcol;

	/* is this the current row ? */
	char crow;

	char text[];
} E_ScreenLine;

typedef struct _E_Screen {
	E_ScreenLine **vscr;
	E_ScreenLine **pscr;

	/* screen size, number of row and columns. */
	int nrow;
	int ncol;

	/* current position of the cursor. */
	int row;
	int col;
} E_Screen;

E_Screen *e_screen_init(E_Term *tr);
void e_screen_free(E_Screen *sc);

void e_screen_putc(E_Screen *sc, int c, char fcol, char bcol);
void e_screen_move(E_Screen *sc, int row, int col);
void e_screen_eeol(E_Screen *sc);
void e_screen_eeop(E_Screen *sc);
void e_screen_crow(E_Screen *sc, int row, char crow);
void e_screen_swap(E_Term *tr, E_Screen *sc, int user_force);

#endif /* _ECO_SCREEN_H */
