/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_TERM_H
#define _ECO_TERM_H

typedef struct E_Term {
	char *name;

	int row;
	int col;

	int nrow;
	int ncol;

	int fgcolor;
	int bgcolor;
} E_Term;

/* term colors. */
#define E_TR_BLACK 	0
#define E_TR_RED 	1
#define E_TR_GREEN	2
#define E_TR_YELLOW	3
#define E_TR_BLUE	4
#define E_TR_MAGENTA	5
#define E_TR_CYAN	6
#define E_TR_WHITE	7

/* term characters. */
#define E_TR_BEL 0x07
#define E_TR_ESC 0x1B

E_Term *e_term_open(void);
void e_term_close(E_Term *tr);

int e_term_getc(void);
void e_term_putc(int c);
void e_term_flush(void);

void e_term_fgcol(E_Term *tr, int color);
void e_term_bgcol(E_Term *tr, int color);
void e_term_move(int row, int col);
void e_term_eeol(void);
void e_term_eeop(E_Term *tr);
void e_term_rev(E_Term *tr, int state);
void e_term_beep(void);

#endif /* _ECO_TERM_H */
