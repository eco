/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include "debug.h"
#include "list.h"
#include "time.h"
#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "file.h"
#include "status.h"
#include "update.h"
#include "key.h"
#include "func.h"
#include "cmd.h"
#include "config.h"
#include "kill.h"
#include "syntax.h"


/* Function declaration. */
static int e_cmode_check(char *ext);
static void e_cmode_init(void);
static int e_check_unsave(E_Eco *ec);

static void e_signal(int num);
static void e_signal_size(int num);
static void e_quit(E_Eco *ec);
static void e_keymap_init(void);

/* only global in this file!! */
static E_Eco *E= NULL;


int main(int argc, char **argv)
{
	E_Buffer *bf;
	E_View *v;
	int i, st;

	E= (E_Eco *)malloc(sizeof(E_Eco));
	if (!E) {
		perror("malloc");
		exit(-1);
	}

	e_debug_init();
	e_config_init();

	E->tr= e_term_open();
	E->sc= e_screen_init(E->tr);

	/* handler signal. */
	signal(SIGTERM, e_signal);
	signal(SIGQUIT, e_signal);
	signal(SIGWINCH, e_signal_size);

	/* view list. */
	E->view_list= NULL;

	/* buffers list. */
	E->buffers= NULL;

	argc--;
	argv++;

	for (i= 0; i < argc; i++) {
		/* parse options here!! */
		bf= e_file_read(argv[i], &st);
		if ((!bf) && (!st))
			bf= e_buffer_new(argv[i]);

		if (bf) {
			v= e_view_new(E->sc);
			v->b= bf;
			e_main_view_add(E, v);
			e_main_add(E, bf);
		}
	}

	if (!E->buffers) {
		/* default buffer. */
		bf= e_buffer_new("main");
		v= e_view_new(E->sc);
		v->b= bf;
		e_main_view_add(E, v);
		e_main_add(E, bf);
	}

	/* and add to the view. */
	E->view= E->view_list;
	e_view_show(E->view);
	e_view_redraw(E->view);

	/* name to functions binding. */
	e_func_add("e_quit", e_quit);
	e_func_add("e_cmd_write", e_cmd_write);
	e_func_add("e_cmd_killline", e_cmd_killline);
	e_func_add("e_cmd_rotate", e_cmd_rotate);
	e_func_add("e_cmd_goto", e_cmd_goto);
	e_func_add("e_cmd_cr", e_cmd_cr);
	e_func_add("e_cmd_ht", e_cmd_ht);
	e_func_add("e_cmd_backspace", e_cmd_backspace);
	e_func_add("e_cmd_find_file", e_cmd_find_file);
	e_func_add("e_cmd_cutline", e_cmd_cutline);
	e_func_add("e_cmd_paste", e_cmd_paste);
	e_func_add("e_cmd_cleankill", e_cmd_cleankill);

	/* load user keymap. */
	e_keymap_init();

	/* init the kill buffer. */
	e_kill_init();

	/* default colors. */
	E->fg= E_TR_WHITE;
	E->bg= E_TR_BLACK;

	/* clean term and screen. */
	e_term_move(0, 0);
	e_term_eeop(E->tr);

	e_screen_move(E->sc, 0, 0);
	e_screen_eeop(E->sc);
	e_update(E);
	e_status_draw(E);
	e_screen_swap(E->tr, E->sc, 1);

	/* register the cmode in the system. */
	e_cmode_init();

	/* and start the loop. */
	E->loop= 1;
	E->last_ch= 0;

	while (E->loop) {
		int c;

		/* Get the next character. */
		c= e_term_getc();

		/*
		 * Clear the status message, before
		 * call any other functions.
		 */
		E->view->stmsg= 0;

		/*
		 * Depend on the character we have three possibilities here:
		 *  1) An escape secuence or command.
		 *  2) Combination of command (for example. CTRL X + C)
		 *  3) Normal text input for the buffer.
		 *
		 * First case:
		 */
		if (c == E_TR_ESC) {
			int c, redraw;

			c= e_term_getc();
			redraw= 0;

			if (c == '[') {
				c= e_term_getc();
				if (c == 'D') {
					e_buffer_left(E->view->b);
					redraw= 1;
				}
				else if (c == 'C') {
					e_buffer_right(E->view->b);
					redraw= 1;
				}
				else if (c == 'A') {
					e_buffer_up(E->view->b);
					redraw= 1;
				}
				else if (c == 'B') {
					e_buffer_down(E->view->b);
					redraw= 1;
				}
				else if (c == 'H') {
					e_buffer_bol(E->view->b);
					redraw= 1;
				}
				else if (c == 'F') {
					e_buffer_eol(E->view->b);
					redraw= 1;
				}
				else if (c == '1') {
					c= e_term_getc();
					if (c == '~') {
						e_buffer_bol(E->view->b);
						redraw= 1;
					}
					else if (c == '7') {
						c= e_term_getc();
						if (c == '7') {
							if (E->view->b->dot == E->view->b->line->used) {
								e_buffer_down(E->view->b);
								e_buffer_joinline(E->view->b);
							}
							else
								e_buffer_del(E->view->b);
							redraw= 1;
						}
					}
				}
				else if (c == '3') {
					c= e_term_getc();
					if (c == '~') {
						if (E->view->b->dot == E->view->b->line->used) {
							e_buffer_down(E->view->b);
							e_buffer_joinline(E->view->b);
						}
						else
							e_buffer_del(E->view->b);
						redraw= 1;
					}
				}
				else if (c == '4') {
					c= e_term_getc();
					if (c == '~') {
						e_buffer_eol(E->view->b);
						redraw= 1;
					}
				}
				else if (c == '5') {
					c= e_term_getc();
					if (c == '~') {
						e_buffer_scroll(E->view->b, E->sc->nrow - 3, 1);
						redraw= 1;
					}
				}
				else if (c == '6') {
					c= e_term_getc();
					if (c == '~') {
						e_buffer_scroll(E->view->b, E->sc->nrow - 3, 0);
						redraw= 1;
					}
				}
				else if (c == '[') {
					c= e_term_getc();
					if (c == '3') {
						c= e_term_getc();
						if (c == '~') {
							if (E->view->b->dot == E->view->b->line->used) {
								e_buffer_down(E->view->b);
								e_buffer_joinline(E->view->b);
							}
							else
								e_buffer_del(E->view->b);
							redraw= 1;
						}
					}
				}
			}
			else if (c == 'O') {
				c= e_term_getc();
				if (c == 'F') {
					e_buffer_eol(E->view->b);
					redraw= 1;
				}
				else if (c == 'H') {
					e_buffer_bol(E->view->b);
					redraw= 1;
				}
			}

			if (redraw)
				e_view_redraw(E->view);
		}
		/* Second case here. */
		else if (c > 0x00 && c < 0x20) {
			short ctrl_x;
			char exec_cmd;

			c= c + '@';
			ctrl_x= 0;
			exec_cmd= 1;

			if (c == 'X') {
				c= e_term_getc();
				ctrl_x= 1;

				if (c > 0x00 && c < 0x20)
					c= c + '@';

				/* yes, ctrl+G always cancel the command. */
				if (c == 'G')
					exec_cmd= 0;
			}
			if (exec_cmd)
				e_key_exec(E, c, ctrl_x);
		}
		/* And normal input text for the buffer. */
		else if (c > 0x1f && c <= 0x7f) {
			if (c == 0x7f)
				e_buffer_backspace(E->view->b);
			else
				e_buffer_insert(E->view->b, c, 1);
			e_view_redraw(E->view);

			/* update the last character. */
			E->last_ch= c;
		}

		e_update(E);
		e_status_draw(E);
		e_screen_swap(E->tr, E->sc, 0);
		e_term_flush();

		e_sleep_ms(5);
	}

	while (E->buffers) {
		/*
		 * Don't need check for changes, the e_quit
		 * function already handler it.
		 */
		bf= E->buffers->next;
		e_buffer_free(E->buffers);
		E->buffers= bf;
	}

	e_kill_end();
	e_key_free();

	while (E->view_list) {
		v= E->view_list->next;
		e_view_free(E->view_list);
		E->view_list= v;
	}

	e_screen_free(E->sc);
	e_term_close(E->tr);
	free((void *)E);
	e_debug_end();

	return(0);
}

static int e_check_unsave(E_Eco *ec)
{
	E_Buffer *p;
	char *s, *buf;

	buf= (char *)malloc(512);
	p= ec->buffers;
	while (p) {
		if (!strcmp(p->paths->file, "main")) {
			p= p->next;
			continue;
		}

		if (p->flag & BUFFER_FLUSH) {
			sprintf(buf, "Modified buffer (%s), save (y/n/c)? ", p->paths->file);
			while (!(s= e_status_get_msg(ec, buf)))
				;

			if (s[0] == 'y')
				e_file_write(ec, p);
			else if (s[0] == 'c') {
				free((void *)buf);
				return(1);
			}
		}
		p= p->next;
	}
	free((void *)buf);
	return(0);
}

static void e_signal(int num)
{
	E->loop= 0;
}

static void e_signal_size(int num)
{
	struct winsize ws;
	E_View *v;

	if (ioctl(0, TIOCGWINSZ, &ws))
		return;

	if (ws.ws_row-1 == E->tr->nrow && ws.ws_col == E->tr->ncol)
		return;

	E->tr->nrow= ws.ws_row-1;
	E->tr->ncol= ws.ws_col;

	/* yes... fix this. */
	e_screen_free(E->sc);
	E->sc= e_screen_init(E->tr);

	/* ok, we need resize all the view. */
	v= E->view_list;
	while (v) {
		e_view_resize(v, E->sc->nrow, E->sc->ncol);
		v= v->next;
	}

	/* clearn term and screen. */
	e_term_move(0, 0);
	e_term_eeop(E->tr);

	e_screen_move(E->sc, 0, 0);
	e_screen_eeop(E->sc);
	e_update(E);
	e_status_draw(E);
	e_screen_swap(E->tr, E->sc, 1);
	e_term_flush();
	signal(SIGWINCH, e_signal_size);
}

static void e_quit(E_Eco *ec)
{
	if (e_check_unsave(ec))
		return;
	E->loop= 0;
}

static void e_keymap_init(void)
{
	e_key_add('C', 1, e_quit);
	e_key_add('S', 1, e_cmd_write);
	e_key_add('K', 0, e_cmd_killline);
	e_key_add('R', 0, e_cmd_rotate);
	e_key_add('G', 0, e_cmd_goto);
	e_key_add('F', 1, e_cmd_find_file);
	e_key_add('U', 0, e_cmd_update);
	e_key_add('S', 0, e_cmd_search_forward);
	e_key_add('B', 0, e_cmd_search_backward);
	e_key_add('R', 1, e_cmd_replace);

	e_key_add('M', 0, e_cmd_cr);
	e_key_add('I', 0, e_cmd_ht);
	e_key_add('H', 0, e_cmd_backspace);
	e_key_add('C', 0, e_cmd_cutline);
	e_key_add('Y', 0, e_cmd_paste);
	e_key_add('L', 0, e_cmd_cleankill);
	e_key_add('V', 0, e_cmd_copyline);
}

void e_main_rotate(E_Eco *ec)
{
	e_view_unshow(ec->view);
	if (ec->view->next)
		ec->view= ec->view->next;
	else
		ec->view= ec->view_list;

	e_view_show(ec->view);
	e_view_redraw(ec->view);
}

E_Buffer *e_main_find(E_Eco *ec, char *name)
{
	E_File_Path *paths;
	E_Buffer *p;

	/* get the absolute path */
	paths= e_file_get_paths(name);
	if (!paths)
		return(NULL);

	if (!paths->path) {
		free((void *)paths);
		return(NULL);
	}

	if (!paths->file) {
		free((void *)paths->path);
		free((void *)paths);
		return(NULL);
	}

	p= ec->buffers;
	while (p) {
		if (!strcmp(p->paths->file, paths->file)) {
			free((void *)paths->path);
			free((void *)paths->file);
			free((void *)paths);
			return(p);
		}
		p= p->next;
	}
	free((void *)paths->path);
	free((void *)paths->file);
	free((void *)paths);
	return(NULL);
}

void e_main_add(E_Eco *ec, E_Buffer *bf)
{
	bf->next= ec->buffers;
	ec->buffers= bf;
}

void e_main_remove(E_Eco *ec, E_Buffer *bf)
{
	E_Buffer *p, *p1;

	p= ec->buffers;
	p1= NULL;
	while (p) {
		if (p == bf) {
			if (p1)
				p1->next= p->next;
			else
				ec->buffers= p->next;

			bf->next= NULL;
			return;
		}
		p1= p;
		p= p->next;
	}
}

void e_main_view_add(E_Eco *ec, E_View *v)
{
	v->next= ec->view_list;
	v->prev= NULL;
	if (ec->view_list)
		ec->view_list->prev= v;
	ec->view_list= v;
}

void e_main_view_rem(E_Eco *ec, E_View *v)
{
	if (v->prev)
		v->prev->next= v->next;
	if (v->next)
		v->next->prev= v->prev;

	if (v == ec->view_list) {
		if (v->next)
			ec->view_list= v->next;
		else
			ec->view_list= v->prev;
	}
}

E_View *e_main_view_find(E_Eco *ec, E_Buffer *b)
{
	E_View *v;

	v= ec->view_list;
	while (v) {
		if (v->b == b)
			return(v);
		v= v->next;
	}
	return(NULL);
}

/* make a C dictionary! */
static int e_cmode_check(char *ext)
{
	if (!strcmp(ext, ".c"))
		return(0);
	else if (!strcmp(ext, ".C"))
		return(0);
	return(1);
}

static void e_cmode_init(void)
{
	E_Syntax *sy;

	sy= e_syntax_new();
	sy->name= strdup("C Mode");
	sy->check_type= e_cmode_check;
	sy->dirs.first= NULL;
	sy->dirs.last= NULL;

	e_syntax_word_add(sy, "void", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "int", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "char", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "float", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "double", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "long", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "struct", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "typedef", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "static", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "inline", E_TR_RED, E_TR_BLACK);

	e_syntax_word_add(sy, "if", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "else", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "while", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "for", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "do", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "return", E_TR_BLUE, E_TR_BLACK);
	e_syntax_word_add(sy, "ifndef", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "define", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "endif", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "include", E_TR_GREEN, E_TR_BLACK);
	e_syntax_word_add(sy, "malloc", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "free", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "realloc", E_TR_RED, E_TR_BLACK);
	e_syntax_word_add(sy, "++", E_TR_CYAN, E_TR_BLACK);
	e_syntax_word_add(sy, "--", E_TR_CYAN, E_TR_BLACK);

	e_syntax_char_add(sy, '{', E_TR_YELLOW, E_TR_BLACK);
	e_syntax_char_add(sy, '}', E_TR_YELLOW, E_TR_BLACK);

	e_syntax_char_add(sy, '(', E_TR_YELLOW, E_TR_BLACK);
	e_syntax_char_add(sy, ')', E_TR_YELLOW, E_TR_BLACK);
	e_syntax_char_add(sy, '[', E_TR_YELLOW, E_TR_BLACK);
	e_syntax_char_add(sy, ']', E_TR_YELLOW, E_TR_BLACK);

	e_syntax_char_add(sy, '=', E_TR_GREEN, E_TR_BLACK);
	e_syntax_char_add(sy, ',', E_TR_GREEN, E_TR_BLACK);
	e_syntax_char_add(sy, '.', E_TR_GREEN, E_TR_BLACK);
	e_syntax_char_add(sy, ';', E_TR_GREEN, E_TR_BLACK);
	e_syntax_char_add(sy, '*', E_TR_YELLOW, E_TR_BLACK);
	e_syntax_char_add(sy, '/', E_TR_YELLOW, E_TR_BLACK);
	e_syntax_char_add(sy, '"', E_TR_GREEN, E_TR_BLACK);
	e_syntax_char_add(sy, '+', E_TR_CYAN, E_TR_BLACK);
	e_syntax_char_add(sy, '-', E_TR_CYAN, E_TR_BLACK);
	e_syntax_char_add(sy, '>', E_TR_CYAN, E_TR_BLACK);
	e_syntax_char_add(sy, '<', E_TR_CYAN, E_TR_BLACK);

	e_syntax_add(sy);
}
