/*
 * Copyright (C) 2009 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_LIST_H
#define _ECO_LIST_H

typedef struct _E_Link {
	struct _E_Link *next;
	struct _E_Link *prev;
} E_Link;

typedef struct _E_List {
	E_Link *first;
	E_Link *last;
} E_List;

#define E_LIST_INIT(l) \
l.first= NULL; \
l.last= NULL

#define E_LIST_ADD(l, n) e_list_add(l, (E_Link *)n)
#define E_LIST_HEAD(l, n) e_list_head(l, (E_Link *)n)
#define E_LIST_REM(l, n) e_list_rem(l, (E_Link *)n)

void e_list_add(E_List *list, E_Link *ln);
void e_list_head(E_List *list, E_Link *ln);
void e_list_rem(E_List *list, E_Link *ln);

#endif /* _ECO_LIST_H */
