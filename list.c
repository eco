/*
 * Copyright (C) 2009 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>

#include "list.h"


void e_list_add(E_List *list, E_Link *ln)
{
	ln->next= NULL;
	ln->prev= list->last;

	if (list->last)
		list->last->next= ln;
	if (!list->first)
		list->first= ln;
	list->last= ln;
}

void e_list_head(E_List *list, E_Link *ln)
{
	ln->next= list->first;
	ln->prev= NULL;

	if (list->first)
		list->first->prev= ln;
	if (!list->last)
		list->last= ln;
	list->first= ln;
}

void e_list_rem(E_List *list, E_Link *ln)
{
	if (ln->prev)
		ln->prev->next= ln->next;
	if (ln->next)
		ln->next->prev= ln->prev;
	if (list->first == ln)
		list->first= ln->next;
	if (list->last == ln)
		list->last= ln->prev;
	ln->next= NULL;
	ln->prev= NULL;
}
