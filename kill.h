/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_KILL_H
#define _ECO_KILL_H

void e_kill_init(void);
void e_kill_end(void);

void e_kill_cut(E_Line *ln);
void e_kill_paste(E_Buffer *bf);
void e_kill_clean(void);

#endif /* _ECO_KILL_H */
