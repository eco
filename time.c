/*
 * Copyright (C) 2009 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>


/* return the time in seconds. */
double e_time_get(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return((double)tv.tv_sec + (((double)tv.tv_usec)/10000.0));
}

/* sleep 'ms' miliseconds. */
void e_sleep_ms(int ms)
{
	if (ms >= 1000) {
		sleep(ms/1000);
		ms= (ms%1000);
	}
	usleep(ms*1000);
}
