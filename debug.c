/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdarg.h>


#ifdef WITH_DEBUG

/* debug file. */
FILE *_dbg_fp= NULL;

#endif

void e_debug_init(void)
{
#ifdef WITH_DEBUG
	_dbg_fp= fopen("eco.debug", "w");
	if (_dbg_fp)
		setvbuf(_dbg_fp, NULL, _IONBF, 0);
#endif
}

void e_debug_end(void)
{
#ifdef WITH_DEBUG
	if (_dbg_fp) {
		fclose(_dbg_fp);
		_dbg_fp= NULL;
	}
#endif
}

void e_debug_printf(char *fmt, ...)
{
#ifdef WITH_DEBUG
	va_list ap;

	if (!_dbg_fp)
		return;

	va_start(ap, fmt);
	vfprintf(_dbg_fp, fmt, ap);
	va_end(ap);
#endif
}
