/*
 * Copyright (C) 2009 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_TIME_H
#define _ECO_TIME_H

double e_time_get(void);
void e_sleep_ms(int ms);

#endif /* _ECO_TIME_H */
