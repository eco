/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_BUFFER_H
#define _ECO_BUFFER_H

struct E_File_Path;

typedef struct _E_Line {
	struct _E_Line *next;
	struct _E_Line *prev;

	/* line flags. */
	char flag;

	/* chunk of text. */
	char *text;

	/* chunk size. */
	int size;

	/* memory used. */
	int used;
} E_Line;

typedef struct _E_Buffer {
	struct _E_Buffer *next;

	/* file paths. */
	struct E_File_Path *paths;

	/* file name or untitled */
	char *name;

	/* line list. */
	E_Line *lines;

	/* "top-level" line. */
	E_Line *first;

	/* current "edit" line. */
	E_Line *line;

	/* cursor position in the "current line" */
	short dot;

	/* real pad for big lines. */
	short dot_pad;

	/* buffer flags. */
	char flag;

	/* number of lines in this buffer. */
	int nlines;

	/* current line number. */
	int nl;
} E_Buffer;

/* buffer->flag */
#define BUFFER_FLUSH 	(1<<0)
#define BUFFER_CMODE	(1<<1)
#define BUFFER_UP	(1<<2)
#define BUFFER_DOWN	(1<<3)
#define BUFFER_NOIDENT	(1<<4)

/* buffer search mode. */
#define BUFFER_SEARCH_FORWARD 0
#define BUFFER_SEARCH_BACKWARD 1

/* set/unset buffer flags */
#define BUFFER_SET(b, f) \
if (!(b->flag & f)) \
	b->flag|= (f)

#define BUFFER_UNSET(b, f) \
if (b->flag & f) \
	b->flag&= ~(f)

E_Buffer *e_buffer_new(char *file);
void e_buffer_free(E_Buffer *bf);

void e_buffer_newline(E_Buffer *bf);
void e_buffer_newline_first(E_Buffer *bf);
void e_buffer_joinline(E_Buffer *bf);
void e_buffer_splitline(E_Buffer *bf);
void e_buffer_cleanline(E_Buffer *bf);
void e_buffer_insert(E_Buffer *bf, int c, int repeat);
void e_buffer_insert_str(E_Buffer *bf, char *str, int len);
void e_buffer_backspace(E_Buffer *bf);
void e_buffer_del(E_Buffer *bf);
void e_buffer_killline(E_Buffer *bf, int cut);

void e_buffer_up(E_Buffer *bf);
void e_buffer_down(E_Buffer *bf);
void e_buffer_left(E_Buffer *bf);
void e_buffer_right(E_Buffer *bf);

void e_buffer_goto(E_Buffer *bf, int line);
void e_buffer_goto_begin(E_Buffer *bf);
void e_buffer_goto_end(E_Buffer *bf);
void e_buffer_bol(E_Buffer *bf);
void e_buffer_eol(E_Buffer *bf);
void e_buffer_scroll(E_Buffer *bf, int nline, int dir);

void e_buffer_search(E_Buffer *bf, char *pattern, int dir);
int e_buffer_replace(E_Buffer *bf, char *pattern, char *replace);

void e_buffer_rem_empty_lines(E_Buffer *bf);

#endif /* _ECO_BUFFER_H */
