/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_DEBUG_H
#define _ECO_DEBUG_H

#include <stdarg.h>

void e_debug_init(void);
void e_debug_end(void);
void e_debug_printf(char *fmt, ...);

#endif /* _ECO_DEBUG_H */
