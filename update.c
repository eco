/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>

#include "debug.h"
#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "config.h"
#include "eco.h"


int e_update_get_tab(E_Eco *ec, int col)
{
	int tab;

	tab= 8 + ec->view->col;
	while (tab <= col)
		tab+= 8;
	return(tab);
}

int e_update_line(E_Eco *ec, E_Line *ln, int row)
{
	int i, col, tab, invert;
	char fgcol, bgcol;

	if (ec->view->b->line == ln) {
		/* Mark the current line in the screen. */
		e_screen_crow(ec->sc, row, 1);
		invert= 1;
		i= ec->view->b->dot_pad;
	}
	else {
		/* clear the flag if this is not the current line. */
		e_screen_crow(ec->sc, row, 0);
		invert= 0;
		i= 0;
	}

	if (invert) {
		fgcol= E_TR_BLACK;
		bgcol= E_TR_GREEN;
	}
	else {
		fgcol= E_TR_WHITE;
		bgcol= E_TR_BLACK;
	}

	for (col= ec->view->col; col < ec->view->rcol && i < ln->used; i++, col++) {
		if (ln->text[i] == '\t') {
			tab= e_update_get_tab(ec, col);
			while (col != tab) {
				e_screen_move(ec->sc, row, col);
				e_screen_putc(ec->sc, ' ', fgcol, bgcol);
				col++;
			}
			col--;
		}
		else {
			e_screen_move(ec->sc, row, col);
			if (ln->text[i] >= 0x20 && ln->text[i] < 0x7f)
				e_screen_putc(ec->sc, ln->text[i], fgcol, bgcol);
			else
				e_screen_putc(ec->sc, ' ', fgcol, bgcol);
		}
	}

	/* clean the rest of the line. */
	if (col < ec->view->rcol) {
		e_screen_move(ec->sc, row, col);
		e_screen_eeol(ec->sc);
	}
	return(row);
}

static int __get_real_col(E_Eco *ec, E_Line *ln, int dot)
{
	int col, i;

	for (i= dot, col= ec->view->col; i < ec->view->b->dot; i++) {
		if (ec->view->b->line->text[i] == '\t')
			col= e_update_get_tab(ec, col);
		else
			col++;
	}
	return(col);
}

static void __reframe(E_Eco *ec)
{
	E_Line *p;
	int row, col, found, i;

	/* check if we need pad the dot. */
	col= __get_real_col(ec, ec->view->b->line, 0);
	ec->view->b->dot_pad= 0;

	while (col > ec->view->rcol) {
		/* ok we need pad this, but go one-by-one so the
		 * left/right keys work fine.
		 */
		ec->view->b->dot_pad++;
		col= __get_real_col(ec, ec->view->b->line, ec->view->b->dot_pad);
	}

	p= ec->view->b->first;
	row= ec->view->row;
	found= 0;
	while (p) {
		if (p == ec->view->b->line) {
			found= 1;
			break;
		}

		row++;
		p= p->next;
	}

	e_debug_printf("Check if need reframe\n");
	/*
	 * This can happen, the current line is before
	 * the first line and from the first to the end
	 * is < nrow, so need be sure.
	 */
	if ((row < ec->view->rrow) && (found))
		return;

	/* We have 3 case here.
	 *  1) The current line is out of the screen because a "up"
	 *  2) The current line is out of the screen because a "down"
	 *  3) A scroll up/down
	 */
	if (ec->view->b->flag & BUFFER_UP) {
		/* in this case, the prev line of the current
		 * line is where we need to go.
		 */
		if (ec->view->b->first->prev)
			ec->view->b->first= ec->view->b->first->prev;
		e_debug_printf("Reframe up buffer\n");
		BUFFER_UNSET(ec->view->b, BUFFER_UP);
	}
	else if (ec->view->b->flag & BUFFER_DOWN) {
		/* in this case the next line of the last line
		 * is our target, so just move one line ;)
		 */
		if (ec->view->b->first->next)
			ec->view->b->first= ec->view->b->first->next;
		e_debug_printf("Reframe down buffer\n");
		BUFFER_UNSET(ec->view->b, BUFFER_DOWN);
	}
	else {
		/* Ok, we are here because page up/down, the normal
		 * scroll with 3+ lines.
		 */
		ec->view->b->first= ec->view->b->line;
		for (i= 0; i < 3; i++) {
			if (!ec->view->b->first->prev)
				break;
			ec->view->b->first= ec->view->b->first->prev;
		}
		e_debug_printf("Reframe buffer\n");
	}
}

void e_update_cursor(E_Eco *ec)
{
	E_Line *p;
	int row, col;

	/*
	 * This function sync the physical and virtual
	 * cursor position, assume that the buffer don't
	 * need reframe.
	 *
	 * First, match the current row.
	 */
	row= ec->view->row;
	p= ec->view->b->first;
	while (p) {
		if (p == ec->view->b->line)
			break;

		row++;
		p= p->next;
	}

	/* second check if the line is "extend" or not. */
	col= __get_real_col(ec, ec->view->b->line, 0);

	if (col > ec->view->rcol)
		col= __get_real_col(ec, ec->view->b->line, ec->view->b->dot_pad);

	e_screen_move(ec->sc, row, col);
	e_term_move(row, col);
}

void e_update(E_Eco *ec)
{
	E_View *v, *vp;
	E_Line *ln;
	char fgcol, bgcol;
	int row;

	fgcol= e_config_get_char("Screen", "empty_char_fg");
	bgcol= e_config_get_char("Screen", "empty_char_bg");
	if (fgcol == -1)
		fgcol= E_TR_BLUE;
	if (bgcol == -1)
		bgcol= E_TR_BLACK;

	/* first save the active view. */
	v= ec->view;

	/* now update all the view that are show and need redraw. */
	vp= ec->view_list;
	while (vp) {
		if ((vp->flag & VIEW_REDRAW) && (vp->flag & VIEW_SHOW)) {
			/* make the active, just for draw. */
			ec->view= vp;

			/* check for scroll. */
			__reframe(ec);

			ln= ec->view->b->first;
			row= ec->view->row;
			while (ln && (row < ec->view->rrow)) {
				row= e_update_line(ec, ln, row);
				row++;
				ln= ln->next;
			}

			/* fill the unused lines. */
			while (row < ec->view->rrow) {
				e_screen_move(ec->sc, row, ec->view->col);
				e_screen_putc(ec->sc, '~', E_TR_BLUE, E_TR_BLACK);
				e_screen_move(ec->sc, row, ec->view->col+1);
				e_screen_eeol(ec->sc);
				row++;
			}

			/* always remove the scroll flags or
			 * we can get bad result in the next reframe.
			 */
			if (vp->b->flag & BUFFER_UP) {
				BUFFER_UNSET(vp->b, BUFFER_UP);
			}
			if (vp->b->flag & BUFFER_DOWN) {
				BUFFER_UNSET(vp->b, BUFFER_DOWN);
			}
			vp->flag &= ~VIEW_REDRAW;
		}
		vp= vp->next;
	}

	/* restore the active view. */
	ec->view= v;
	e_update_cursor(ec);
}
