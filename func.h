/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_FUNC_H
#define _ECO_FUNC_H

typedef struct _E_Function {
	struct _E_Function *next;

	/* function name. */
	char *name;

	/* pointer to the function. */
	void (*func)(E_Eco *);
} E_Function;

void e_func_free(void);
E_Function *e_func_find(char *name);
void e_func_add(char *name, void (*func)(E_Eco *));
void e_func_remove(char *name);

#endif /* _ECO_FUNC_H */
