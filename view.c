/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "config.h"


E_View *e_view_new(E_Screen *sc)
{
	E_View *v;

	v= (E_View *)malloc(sizeof(E_View));
	v->next= NULL;
	v->prev= NULL;

	/* by default, all the screen. */
	v->row= 0;
	v->col= 0;
	v->nrow= sc->nrow;
	v->ncol= sc->ncol;
	v->rrow= sc->nrow;
	v->rcol= sc->ncol;
	v->b= NULL;
	v->search[0]= '\0';

	v->stfg= e_config_get_int("View", "foreground");
	v->stbg= e_config_get_int("View", "background");
	if (v->stfg == -1)
		v->stfg= E_TR_GREEN;
	if (v->stbg == -1)
		v->stbg= E_TR_BLACK;

	v->stbuf[0]= '\0';
	v->stcol= v->col;
	v->strow= v->rrow;
	v->stused= 0;
	v->stmsg= 0;
	v->flag= 0;
	return(v);
}

void e_view_free(E_View *v)
{
	free((void *)v);
}

void e_view_move(E_View *v, int row, int col)
{
	v->row= row;
	v->col= col;
	v->rrow= row + v->nrow;
	v->rcol= col;
	v->strow= v->rrow;
	v->stcol= v->rcol;
	e_view_redraw(v);
}

void e_view_resize(E_View *v, int nrow, int ncol)
{
	v->nrow= nrow;
	v->ncol= ncol;
	v->rrow= v->row + nrow;
	v->rcol= v->col + ncol;
	v->strow= v->rrow;
	v->stcol= v->col;
	e_view_redraw(v);
}

void e_view_redraw(E_View *v)
{
	if (!(v->flag & VIEW_REDRAW))
		v->flag |= VIEW_REDRAW;
}

void e_view_show(E_View *v)
{
	if (!(v->flag & VIEW_SHOW))
		v->flag |= VIEW_SHOW;
}

void e_view_unshow(E_View *v)
{
	if (v->flag & VIEW_SHOW)
		v->flag &= ~VIEW_SHOW;
}
