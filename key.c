/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>

#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "status.h"
#include "key.h"


/* key list. */
E_Key *keys= NULL;

void e_key_free(void)
{
	E_Key *p;

	while (keys) {
		p= keys->next;
		free((void *)keys);
		keys= p;
	}
}

E_Key *__e_key_find(short code, short ctrl_x)
{
	E_Key *p;

	p= keys;
	while (p) {
		if (p->code == code && p->ctrl_x == ctrl_x)
			return(p);
		p= p->next;
	}
	return(NULL);
}

void e_key_add(short code, short ctrl_x, void (*cb)(E_Eco *))
{
	E_Key *key= __e_key_find(code, ctrl_x);

	if (key)
		return;

	key= (E_Key *)malloc(sizeof(E_Key));
	if (!key)
		return;

	key->next= keys;
	key->ctrl_x= ctrl_x;
	key->code= code;
	key->exec= cb;
	keys= key;
}

void e_key_remove(short code, short ctrl_x)
{
	E_Key *p, *p1;

	p= keys;
	p1= NULL;
	while (p) {
		if (p->code == code && p->ctrl_x == ctrl_x) {
			if (p1)
				p1->next= p->next;
			else
				keys= p->next;

			free((void *)p);
			return;
		}
		p1= p;
		p= p->next;
	}
}

int e_key_exec(E_Eco *ec, short code, short ctrl_x)
{
	E_Key *key;

	key= __e_key_find(code, ctrl_x);
	if (key) {
		(*key->exec) (ec);
		return(0);
	}
	return(1);
}
