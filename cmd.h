/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_CMD_H
#define _ECO_CMD_H

void e_cmd_cr(E_Eco *ec);
void e_cmd_ht(E_Eco *ec);
void e_cmd_backspace(E_Eco *ec);

void e_cmd_write(E_Eco *ec);
void e_cmd_killline(E_Eco *ec);
void e_cmd_rotate(E_Eco *ec);
void e_cmd_goto(E_Eco *ec);
void e_cmd_find_file(E_Eco *ec);

void e_cmd_cutline(E_Eco *ec);
void e_cmd_copyline(E_Eco *ec);
void e_cmd_paste(E_Eco *ec);
void e_cmd_cleankill(E_Eco *ec);

void e_cmd_update(E_Eco *ec);

void e_cmd_search_forward(E_Eco *ec);
void e_cmd_search_backward(E_Eco *ec);
void e_cmd_replace(E_Eco *ec);

void e_cmd_split(E_Eco *ec);
void e_cmd_change_view(E_Eco *ec);

#endif /* _ECO_CMD_H */
