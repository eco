/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_FILE_H
#define _ECO_FILE_H

typedef struct E_File_Path {
	/* absolute path to the directory. */
	char *path;

	/* absolute path to the file. */
	char *file;

	/* lock file. */
	char *lock_file;

	/* true if we lock the file. */
	int locked;
} E_File_Path;

/* Get the absolute path to a file. */
E_File_Path *e_file_get_paths(char *file);

/* Check if the file is locked, return 1 if it's locked or zero. */
int e_file_is_lock(E_File_Path *paths);

/* Lock/Unlock the file. */
void e_file_lock(E_File_Path *paths);
void e_file_lock_rem(E_File_Path *paths);

E_Buffer *e_file_read(char *file, int *status);
void e_file_write(E_Eco *ec, E_Buffer *bf);

#endif /* _ECO_FILE_H */
