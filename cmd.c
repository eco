/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "file.h"
#include "status.h"
#include "cmd.h"
#include "kill.h"
#include "update.h"


void e_cmd_cr(E_Eco *ec)
{
	if (ec->view->b->dot == ec->view->b->line->used)
		e_buffer_newline(ec->view->b);
	else if (ec->view->b->dot == 0) {
		if (ec->view->b->line->prev) {
			e_buffer_up(ec->view->b);
			e_buffer_eol(ec->view->b);
			e_buffer_newline(ec->view->b);
			e_buffer_down(ec->view->b);
		}
		else {
			e_buffer_newline_first(ec->view->b);
		}
	}
	else {
		e_buffer_splitline(ec->view->b);
	}
	e_view_redraw(ec->view);
}

void e_cmd_ht(E_Eco *ec)
{
	e_buffer_insert(ec->view->b, '\t', 1);
	e_view_redraw(ec->view);
}

void e_cmd_backspace(E_Eco *ec)
{
	e_buffer_backspace(ec->view->b);
	e_view_redraw(ec->view);
}

void e_cmd_write(E_Eco *ec)
{
	char *buf;

	if (!strcmp(ec->view->b->paths->file, "main")) {
		buf= e_status_get_msg(ec, "File: ");
		if (!buf)
			return;

		free((void *) ec->view->b->paths->file);
		ec->view->b->paths->file= strdup(buf);
	}

	/* Remove empty lines before save. */
	e_buffer_rem_empty_lines(ec->view->b);

	/* and write the full file to disk. */
	e_file_write(ec, ec->view->b);
}

void e_cmd_killline(E_Eco *ec)
{
	e_buffer_killline(ec->view->b, 0);
	e_view_redraw(ec->view);
}

void e_cmd_rotate(E_Eco *ec)
{
	e_main_rotate(ec);
}

void e_cmd_goto(E_Eco *ec)
{
	char *buf;
	int line;

	buf= e_status_get_msg(ec, "Line: ");
	if (buf) {
		line= atoi(buf);
		e_buffer_goto(ec->view->b, line);
		e_view_redraw(ec->view);
	}
}

void e_cmd_find_file(E_Eco *ec)
{
	E_Buffer *bf;
	E_View *v;
	char *buf;
	int st;

	buf= e_status_get_msg(ec, "Find File: ");
	if (buf) {
		bf= e_main_find(ec, buf);
		if (bf) {
			v= e_main_view_find(ec, bf);
			e_view_unshow(ec->view);
			e_view_show(v);
			e_view_redraw(v);
			ec->view= v;
		}
		else {
			bf= e_file_read(buf, &st);
			if (bf) {
				v= e_view_new(ec->sc);
				v->b= bf;
				e_main_view_add(ec, v);
				e_main_add(ec, bf);

				e_view_show(v);
				e_view_redraw(v);
				e_view_unshow(ec->view);
				ec->view= v;
			}
			else if (st == 2)
				e_status_set_msg(ec, "(file is locked!)");
			else if (st == 1)
				e_status_set_msg(ec, "(file is not a regular file!)");
		}
	}
}

void e_cmd_cutline(E_Eco *ec)
{
	e_buffer_killline(ec->view->b, 1);
	e_view_redraw(ec->view);
}

void e_cmd_copyline(E_Eco *ec)
{
	e_buffer_killline(ec->view->b, 2);
	e_buffer_down(ec->view->b);
	e_view_redraw(ec->view);
}

void e_cmd_paste(E_Eco *ec)
{
	e_kill_paste(ec->view->b);
	e_view_redraw(ec->view);
}

void e_cmd_cleankill(E_Eco *ec)
{
	e_kill_clean();
}

void e_cmd_update(E_Eco *ec)
{
	e_view_redraw(ec->view);
	e_update(ec);
	e_status_draw(ec);
	e_screen_swap(ec->tr, ec->sc, 1);
	e_term_flush();
}

void e_cmd_search_forward(E_Eco *ec)
{
	char *buf;

	buf= e_status_get_msg(ec, "Search: ");
	if (buf) {
		strcpy(ec->view->search, buf);
		e_buffer_search(ec->view->b, ec->view->search, BUFFER_SEARCH_FORWARD);
		e_status_set_msg(ec, "(Search: %s)", ec->view->search);
		e_view_redraw(ec->view);
	}
	else if (ec->view->search) {
		e_buffer_search(ec->view->b, ec->view->search, BUFFER_SEARCH_FORWARD);
		e_status_set_msg(ec, "(Search: %s)", ec->view->search);
		e_view_redraw(ec->view);
	}
}

void e_cmd_search_backward(E_Eco *ec)
{
	char *buf;

	buf= e_status_get_msg(ec, "Search: ");
	if (buf) {
		strcpy(ec->view->search, buf);
		e_buffer_search(ec->view->b, ec->view->search, BUFFER_SEARCH_BACKWARD);
		e_status_set_msg(ec, "(Search: %s)", ec->view->search);
		e_view_redraw(ec->view);
	}
	else if (ec->view->search) {
		e_buffer_search(ec->view->b, ec->view->search, BUFFER_SEARCH_BACKWARD);
		e_status_set_msg(ec, "(Search: %s)", ec->view->search);
		e_view_redraw(ec->view);
	}
}

void e_cmd_replace(E_Eco *ec)
{
	char *buf;
	int num;

	buf= e_status_get_msg(ec, "Replace: ");
	if (buf) {
		strcpy(ec->view->search, buf);
		buf= e_status_get_msg(ec, "With: ");
		if (buf) {
			num= e_buffer_replace(ec->view->b, ec->view->search, buf);
			e_status_set_msg(ec, "(%d subsitutions)", num);
			if (num)
				e_view_redraw(ec->view);
		}
	}
}
