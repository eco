/*
 * Copyright (C) 2008 - 2012 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.h"
#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "kill.h"
#include "file.h"


E_Line *e_line_alloc(void)
{
	E_Line *ln;

	ln= (E_Line *)malloc(sizeof(E_Line));
	ln->next= NULL;
	ln->prev= NULL;
	ln->flag= 0;
	ln->size= 16;
	ln->used= 0;
	ln->text= (char *)malloc(16);
	return(ln);
}

void e_line_realloc(E_Line *ln)
{
	ln->size+= 16;
	ln->text= (char *)realloc((void *)ln->text, ln->size);
}

void e_line_free(E_Line *ln)
{
	free((void *)ln->text);
	free((void *)ln);
}

void e_line_add(E_Buffer *bf, E_Line *ln)
{
	ln->next= bf->line->next;
	ln->prev= bf->line;

	if (ln->next)
		ln->next->prev= ln;
	bf->line->next= ln;
	bf->line= ln;
	bf->nlines++;
}

void e_line_add_first(E_Buffer *bf, E_Line *ln)
{
	ln->next= bf->lines;
	ln->prev= NULL;

	if (ln->next)
		ln->next->prev= ln;

	bf->lines= ln;
	bf->first= ln; /* auto-reframe. */
	bf->nlines++;
}

void e_line_remove(E_Buffer *bf, E_Line *ln)
{
	if (ln->next)
		ln->next->prev= ln->prev;
	if (ln->prev)
		ln->prev->next= ln->next;

	if (bf->lines == ln)
		bf->lines= ln->next;

	if (bf->first == ln) {
		if (bf->first->prev)
			bf->first= bf->first->prev;
		else
			bf->first= bf->first->next;
	}

	if (bf->line == ln) {
		if (bf->line->next)
			bf->line= bf->line->next;
		else
			bf->line= bf->line->prev;
	}

	ln->next= NULL;
	ln->prev= NULL;
	bf->nlines--;
}

E_Buffer *e_buffer_new(char *file)
{
	E_Buffer *bf;

	bf= (E_Buffer *)malloc(sizeof(E_Buffer));
	bf->next= NULL;
	bf->paths= e_file_get_paths(file);

	if (!bf->paths) {
		free((void *)bf);
		return(NULL);
	}

	if (!bf->paths->path) {
		free((void *)bf->paths);
		free((void *)bf);
		return(NULL);
	}

	if (!bf->paths->file) {
		free((void *)bf->paths->path);
		free((void *)bf->paths);
		return(NULL);
	}

	/* remove the full path, only show the name. */
	bf->name= strrchr(bf->paths->file, '/');
	if (!bf->name)
		bf->name= bf->paths->file;
	else
		bf->name++;

	e_debug_printf("path: %s\n", bf->paths->path);
	e_debug_printf("file: %s\n", bf->paths->file);
	e_debug_printf("name: %s\n", bf->name);

	bf->lines= e_line_alloc();
	bf->first= bf->lines;
	bf->line= bf->lines;
	bf->dot= 0;
	bf->dot_pad= 0;
	bf->flag= 0;
	bf->nlines= 1;
	bf->nl= 1;
	return(bf);
}

void e_buffer_free(E_Buffer *bf)
{
	E_Line *p;

	/* only remove the lock when we free the buffer. */
	e_file_lock_rem(bf->paths);

	free((void *)bf->paths->path);
	free((void *)bf->paths->file);
	free((void *)bf->paths->lock_file);
	free((void *)bf->paths);

	while (bf->lines) {
		p= bf->lines->next;
		e_line_free(bf->lines);
		bf->lines= p;
	}
	free((void *)bf);
}

void e_buffer_recalc(E_Buffer *bf)
{
	E_Line *p;

	bf->nl= 1;
	p= bf->lines;
	while (p) {
		if (p == bf->line)
			break;
		bf->nl++;
		p= p->next;
	}
}

/* Add a new line at the current position in the buffer. */
void e_buffer_newline(E_Buffer *bf)
{
	E_Line *ln, *lnp;

	BUFFER_SET(bf, BUFFER_FLUSH);

	/* save the current line. */
	lnp= bf->line;

	ln= e_line_alloc();
	e_line_add(bf, ln);

	/* Always clean the current position, the new line don't
	 * have any text..
	 */
	bf->dot= 0;
	bf->nl++;

	BUFFER_SET(bf, BUFFER_DOWN);

	if (!(bf->flag & BUFFER_NOIDENT)) {
		int i, tot;

		/* Check if we have tabs on the current line
		 * to insert in the next.
		 */
		for (i= 0, tot= 0; i < lnp->used; i++) {
			/* If the line don't have any tab at
			 * the start there is nothing to do.
			 */
			if (i == 0 && lnp->text[i] != '\t')
				break;
			else if (lnp->text[i] == '\t')
				tot++;
			else
				break;
		}

		if (tot) {
			e_buffer_insert(bf, '\t', tot);

			/* If the last line finish with a { add an extra tab. */
			if (lnp->text[lnp->used-1] == '{')
				e_buffer_insert(bf, '\t', 1);

			/* If the last line was empty, remove all the tabs. */
			if (tot == lnp->used)
				lnp->used= 0;
		}
	}
}

/* Add a new line at the begin of the buffer. */
void e_buffer_newline_first(E_Buffer *bf)
{
	E_Line *ln;

	BUFFER_SET(bf, BUFFER_FLUSH);

	ln= e_line_alloc();
	e_line_add_first(bf, ln);
	bf->dot= 0;
	bf->nl++;
}

/* Split the current line in two line, the starting
 * point for split is the current cursor position.
 */
void e_buffer_splitline(E_Buffer *bf)
{
	E_Line *ln;
	int i, dot;

	/* save the current line. */
	ln= bf->line;

	/* and the cursor position. */
	dot= bf->dot;

	/* insert a new line and add all the character
	 * from the current position, to the end.
	 */
	e_buffer_newline(bf);
	for (i= dot; i < ln->used; i++)
		e_buffer_insert(bf, ln->text[i], 1);

	ln->used= dot;

	/* Always go to the begin of the line. */
	bf->dot= 0;
}

/* Join two line, the source line is the current line and
 * the dest line is the prev.
 */
void e_buffer_joinline(E_Buffer *bf)
{
	E_Line *ln;
	int i, dot;

	/* if we are at the begin, return. */
	if (!bf->line->prev)
		return;

	/* save the current line. */
	ln= bf->line;

	/* move to the previous. */
	e_buffer_up(bf);

	/* put the cursor to the end of the line. */
	bf->dot= bf->line->used;

	/* and save the position. */
	dot= bf->dot;

	/* insert all the characters. */
	for (i= 0; i < ln->used; i++)
		e_buffer_insert(bf, ln->text[i], 1);

	/* remove the unused line. */
	e_line_remove(bf, ln);
	e_line_free(ln);

	/* restore the original cursor position. */
	bf->dot= dot;
	e_buffer_recalc(bf);
}

/* Remove the current line from the buffer, this also
 * can save the line in the cut-buffer.
 *
 * If 'cut' is equal to two, only copy the line to the
 * cut-buffer.
 */
void e_buffer_killline(E_Buffer *bf, int cut)
{
	E_Line *ln;

	if (cut) {
		e_kill_cut(bf->line);
		if (cut == 2) {
			if (bf->line->used)
				e_kill_cut(NULL);
			return;
		}
	}

	if ((!bf->line->prev) && (!bf->line->next)) {
		/* If we don't have previous or next line,
		 * the only thing to do is clean the
		 * current line.
		 */
		e_buffer_cleanline(bf);
		return;
	}
	else if (!bf->line->next) {
		/* If we don't have next line, only clean. */
		e_buffer_cleanline(bf);
		return;
	}

	if (bf->line->used) {
		/* This work like emacs, if the line have text
		 * first clean the text.
		 */
		e_buffer_cleanline(bf);
		return;
	}

	/* And if the line don't have text, remove it. */
	BUFFER_SET(bf, BUFFER_FLUSH);

	ln= bf->line;
	e_buffer_down(bf);
	e_line_remove(bf, ln);
	e_line_free(ln);
	e_buffer_recalc(bf);
}

/* Insert a character in the current line at the current
 * cursor position.
 */
void e_buffer_insert(E_Buffer *bf, int c, int repeat)
{
	int i, j;

	BUFFER_SET(bf, BUFFER_FLUSH);

	if (!(bf->flag & BUFFER_NOIDENT)) {
		if (bf->line->text[bf->dot] == ')' && c == ')') {
			if (bf->dot < bf->line->used)
				bf->dot++;
			return;
		}
		else if (bf->line->text[bf->dot] == ';' && c == ';') {
			if (bf->dot < bf->line->used)
				bf->dot++;
			return;
		}
	}

	for (j= 0; j < repeat; j++) {
		/* check if we need reallocate the line. */
		if ((bf->line->used+1) >= bf->line->size)
			e_line_realloc(bf->line);

		/* if the cursor is not at the end of the line,
		 * we need scroll the text to the right.
		 */
		if (bf->dot != bf->line->used) {
			for (i= bf->line->used; i > bf->dot; i--)
				bf->line->text[i]= bf->line->text[i-1];
		}

		/* insert the new character. */
		bf->line->text[bf->dot]= c;
		bf->dot++;
		bf->line->used++;
	}

	if (!(bf->flag & BUFFER_NOIDENT)) {
		if (c == '(') {
			/* save the current position. */
			i= bf->dot;

			/* insert the second part. */
			e_buffer_insert(bf, ')', 1);

			/* and move back the cursor. */
			bf->dot= i;
		}
	}
}

/*
 * Insert multiples characters in the current line at
 * the current cursor position ('\n' are valid here!).
 */
void e_buffer_insert_str(E_Buffer *bf, char *str, int len)
{
	char *p;
	int i;

	BUFFER_SET(bf, BUFFER_FLUSH);

	p= str;
	while (len > 0) {
		if (*p == '\n')
			e_buffer_newline(bf);
		else {
			/* check if we need reallocate the line. */
			if ((bf->line->used+1) >= bf->line->size)
				e_line_realloc(bf->line);

			/* if the cursor is not at the end of the line,
			 * we need scroll the text to the right.
			 */
			if (bf->dot != bf->line->used) {
				for (i= bf->line->used; i > bf->dot; i--)
					bf->line->text[i]= bf->line->text[i-1];
			}

			/* insert the new character. */
			bf->line->text[bf->dot]= *p;
			bf->dot++;
			bf->line->used++;
		}
		p++;
		len--;
	}
}

/* Just that, insert a backspace ;) */
void e_buffer_backspace(E_Buffer *bf)
{
	int i;

	/* The only case that we have here is if the cursor position
	 * is at the begin of the line, in that case we need join
	 * the two lines.
	 */
	if (bf->dot > 0) {
		BUFFER_SET(bf, BUFFER_FLUSH);

		/* If the cursor is not at the end of the line,
		 * we need scroll the text.
		 */
		for (i= bf->dot; i < bf->line->used; i++)
			bf->line->text[i-1]= bf->line->text[i];
		bf->dot--;
		bf->line->used--;
	}
	else
		e_buffer_joinline(bf);
}

void e_buffer_del(E_Buffer *bf)
{
	e_buffer_right(bf);
	e_buffer_backspace(bf);
}

void e_buffer_up(E_Buffer *bf)
{
	if (bf->line->prev) {
		bf->line= bf->line->prev;
		if (bf->dot > bf->line->used)
			bf->dot= bf->line->used;
		bf->nl--;
		BUFFER_SET(bf, BUFFER_UP);
	}
}

void e_buffer_down(E_Buffer *bf)
{
	if (bf->line->next) {
		bf->line= bf->line->next;
		if (bf->dot > bf->line->used)
			bf->dot= bf->line->used;
		bf->nl++;
		BUFFER_SET(bf, BUFFER_DOWN);
	}
}

void e_buffer_left(E_Buffer *bf)
{
	if (bf->dot > 0)
		bf->dot--;
	else {
		e_buffer_up(bf);
		e_buffer_eol(bf);
	}
}

void e_buffer_right(E_Buffer *bf)
{
	if (bf->dot < bf->line->used)
		bf->dot++;
	else {
		e_buffer_down(bf);
		e_buffer_bol(bf);
	}
}

void e_buffer_goto_begin(E_Buffer *bf)
{
	bf->line= bf->lines;
	bf->dot= 0;
	bf->nl= 1;
}

void e_buffer_goto_end(E_Buffer *bf)
{
	E_Line *p;

	p= bf->line;
	while (p->next)
		p= p->next;

	bf->line= p;
	bf->dot= 0;
	bf->nl= bf->nlines+1; /* zero */
}

void e_buffer_bol(E_Buffer *bf)
{
	bf->dot= 0;
}

void e_buffer_eol(E_Buffer *bf)
{
	bf->dot= bf->line->used;
}

void e_buffer_scroll(E_Buffer *bf, int nline, int dir)
{
	int i;

	for(i= 0; i < nline; i++) {
		if (dir) {
			if (!bf->first->prev)
				break;
			bf->first= bf->first->prev;
			if (bf->line->prev)
				bf->line= bf->line->prev;
		}
		else {
			if (!bf->first->next)
				break;
			bf->first= bf->first->next;
			if (bf->line->next)
				bf->line= bf->line->next;
		}
	}
	if (bf->dot > bf->line->used)
		bf->dot= bf->line->used;
	e_buffer_recalc(bf);
}

void e_buffer_goto(E_Buffer *bf, int line)
{
	E_Line *p;
	int num;

	num= 1;
	p= bf->lines;
	while (p) {
		if (num == line)
			break;
		num++;
		p= p->next;
	}

	if (!p)
		return;

	bf->nl= num;
	bf->line= p;
	if (bf->dot > bf->line->used)
		bf->dot= 0;
}

void e_buffer_cleanline(E_Buffer *bf)
{
	BUFFER_SET(bf, BUFFER_FLUSH);

	bf->line->used= 0;
	bf->dot= 0;
}

static E_Line *__e_buffer_search(E_Buffer *bf, char *pattern, int *dot_found, int dir)
{
	E_Line *ln;
	int i, e, a, len, dot, found;

	len= strlen(pattern);
	found= 0;
	dot= 0;

	/* I know.. this is not the best backward.. but work. */
	if (dir == BUFFER_SEARCH_FORWARD)
		ln= bf->line;
	else
		ln= bf->line->prev;

	while (ln) {
		if (dir == BUFFER_SEARCH_FORWARD && ln == bf->line)
			i= bf->dot;
		else
			i= 0;

		for (; i < ln->used; i++) {
			if (ln->text[i] == pattern[0]) {
				/* save the current position of the cursor. */
				a= i;
				dot= i;
				found= 1;

				for (e= 0; e < len; e++) {
					if (ln->text[i] != pattern[e]) {
						found= 0;
						break;
					}

					i++;
					if (i > ln->used) {
						found= 0;
						break;
					}
				}

				i= a;
			}

			if (found)
				break;
		}

		if (found)
			break;

		if (dir == BUFFER_SEARCH_FORWARD)
			ln= ln->next;
		else
			ln= ln->prev;
	}

	if (dir == BUFFER_SEARCH_FORWARD)
		*dot_found= (dot+len);
	else
		*dot_found= dot;

	return(ln);
}

void e_buffer_search(E_Buffer *bf, char *pattern, int dir)
{
	E_Line *ln;
	int dot;

	ln= __e_buffer_search(bf, pattern, &dot, dir);
	if (ln) {
		bf->first= ln;
		bf->line= ln;
		bf->dot= dot;
		e_buffer_recalc(bf);
	}
}

int e_buffer_replace(E_Buffer *bf, char *pattern, char *replace)
{
	E_Line *ln;
	int i, c, len, ncount, dot;

	/* Avoid identation during the replace. */
	bf->flag |= BUFFER_NOIDENT;

	c= strlen(pattern);
	len= strlen(replace);
	ncount= 0;

	ln= __e_buffer_search(bf, pattern, &dot, BUFFER_SEARCH_FORWARD);
	while (ln) {
		/* first set the line, so all the next call
		 * work fine.
		 */
		bf->first= ln;
		bf->line= ln;
		bf->dot= dot;
		e_buffer_recalc(bf);

		/* now remove the pattern. */
		for (i= 0; i < c; i++)
			e_buffer_backspace(bf);

		/* and now insert the new string. */
		for (i= 0; i < len; i++)
			e_buffer_insert(bf, replace[i], 1);

		ncount++;
		ln= __e_buffer_search(bf, pattern, &dot, BUFFER_SEARCH_FORWARD);
	}

	/* and remove the flag. */
	bf->flag &= ~BUFFER_NOIDENT;

	return(ncount);
}

void e_buffer_rem_empty_lines(E_Buffer *bf)
{
	E_Line *ln;
	int i, found, tot;

	ln= bf->lines;
	tot= 0;
	while (ln) {
		found= 1;
		for (i= 0; i < ln->used; i++) {
			if (ln->text[i] != ' ' && ln->text[i] != '\t') {
				found= 0;
				break;
			}
		}

		/* If the line is empty, remove any ' ' or '\t' character. */
		if (found) {
			ln->used= 0;
			tot++;
		}

		ln= ln->next;
	}

	if (tot)
		BUFFER_SET(bf, BUFFER_FLUSH);
}
