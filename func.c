/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "func.h"


/* name to function binding list. */
E_Function *functions= NULL;

void e_func_free(void)
{
	E_Function *p;

	while (functions) {
		p= functions->next;
		free((void *)functions);
		functions= p;
	}
}

E_Function *e_func_find(char *name)
{
	E_Function *p;

	p= functions;
	while (p) {
		if (!strcmp(p->name, name))
			return(p);
		p= p->next;
	}
	return(NULL);
}

void e_func_add(char *name, void (*func)(E_Eco *))
{
	E_Function *fn;

	fn= e_func_find(name);
	if (fn)
		return;

	fn= (E_Function *)malloc(sizeof(E_Function));
	if (!fn)
		return;

	fn->name= name;
	fn->func= func;
	fn->next= functions;
	functions= fn;
}

void e_func_remove(char *name)
{
	E_Function *p, *p1;

	p= functions;
	p1= NULL;
	while (p) {
		if (!strcmp(p->name, name)) {
			if (p1)
				p1->next= p->next;
			else
				functions= p->next;

			free((void *)p);
			return;
		}
		p1= p;
		p= p->next;
	}
}
