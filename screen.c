/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "term.h"
#include "list.h"
#include "screen.h"
#include "syntax.h"


E_Screen *e_screen_init(E_Term *tr)
{
	E_ScreenLine *ln;
	E_Screen *sc;
	int i, e;

	sc= (E_Screen *)malloc(sizeof(E_Screen));
	if (!sc) {
		e_term_close(tr);
		perror("malloc");
		exit (-1);
	}

	sc->vscr= (E_ScreenLine **)malloc(sizeof(E_ScreenLine *) * tr->nrow);
	sc->pscr= (E_ScreenLine **)malloc(sizeof(E_ScreenLine *) * tr->nrow);
	if ((!sc->vscr) || (!sc->pscr)) {
		e_term_close(tr);
		perror("malloc");
		exit(-1);
	}

	sc->nrow= tr->nrow;
	sc->ncol= tr->ncol;
	sc->row= 0;
	sc->col= 0;

	for (i= 0; i < tr->nrow; i++) {
		ln= (E_ScreenLine *)malloc(sizeof(E_ScreenLine)+tr->ncol);
		if (!ln) {
			e_term_close(tr);
			perror("malloc");
			exit(-1);
		}
		ln->fcol= (char *)malloc(tr->ncol);
		ln->bcol= (char *)malloc(tr->ncol);
		ln->crow= 0;
		for (e= 0; e < tr->ncol; e++) {
			ln->fcol[e]= E_TR_WHITE;
			ln->bcol[e]= E_TR_BLACK;
		}
		sc->vscr[i]= ln;

		ln= (E_ScreenLine *)malloc(sizeof(E_ScreenLine)+tr->ncol);
		if (!ln) {
			e_term_close(tr);
			perror("malloc");
			exit(-1);
		}
		ln->crow= 0;
		ln->fcol= (char *)malloc(tr->ncol);
		ln->bcol= (char *)malloc(tr->ncol);
		for (e= 0; e < tr->ncol; e++) {
			ln->fcol[e]= E_TR_WHITE;
			ln->bcol[e]= E_TR_BLACK;
		}
		sc->pscr[i]= ln;
	}
	return(sc);
}

void e_screen_free(E_Screen *sc)
{
	E_ScreenLine *ln;
	int i;

	for (i= 0; i < sc->nrow; i++) {
		ln= sc->vscr[i];
		free((void *)ln->fcol);
		free((void *)ln->bcol);
		free((void *)ln);

		ln= sc->pscr[i];
		free((void *)ln->fcol);
		free((void *)ln->bcol);
		free((void *)ln);
	}

	free((void *)sc->vscr);
	free((void *)sc->pscr);
	free((void *)sc);
}

void e_screen_putc(E_Screen *sc, int c, char fcol, char bcol)
{
	E_ScreenLine *ln;

	if (sc->row >= 0 && sc->row < sc->nrow) {
		ln= sc->vscr[sc->row];
		ln->text[sc->col]= c;
		ln->fcol[sc->col]= fcol;
		ln->bcol[sc->col]= bcol;
	}
}
		
void e_screen_move(E_Screen *sc, int row, int col)
{
	sc->row= row;
	sc->col= col;
}

void e_screen_crow(E_Screen *sc, int row, char crow)
{
	E_ScreenLine *ln;

	if (row >= 0 && row < sc->nrow) {
		ln= sc->vscr[row];
		ln->crow= crow;
	}
}

void e_screen_eeol(E_Screen *sc)
{
	E_ScreenLine *ln;
	int i;

	if (sc->row >= 0 && sc->row < sc->nrow) {
		ln= sc->vscr[sc->row];
		for (i= sc->col; i < sc->ncol; i++) {
			ln->text[i]= ' ';
			ln->fcol[i]= E_TR_WHITE;
			ln->bcol[i]= E_TR_BLACK;
		}
	}
}

void e_screen_eeop(E_Screen *sc)
{
	E_ScreenLine *ln;
	int e, i;

	if (sc->row >= 0 && sc->row < sc->nrow) {
		for(e= 0; e < sc->nrow; e++) {
			ln= sc->vscr[e];
			for (i= 0; i < sc->ncol; i++) {
				ln->text[i]= ' ';
				ln->fcol[i]= E_TR_WHITE;
				ln->bcol[i]= E_TR_BLACK;
			}
		}
	}
}

void e_screen_swap(E_Term *tr, E_Screen *sc, int user_force)
{
	E_ScreenLine *vln, *pln;
	E_Syntax *sy;
	int e, i, force;

	/* FIXME! */
	sy= e_syntax_search(".c");

	/* default color, white and black. */
	e_term_fgcol(tr, E_TR_WHITE);
	e_term_bgcol(tr, E_TR_BLACK);

	for(e= 0; e < sc->nrow; e++) {
		vln= sc->vscr[e];
		pln= sc->pscr[e];
		force= user_force;

		/* Apply the color but skip current row. */
		if (sy && vln->crow == 0)
			e_syntax_apply(sy, vln, sc->ncol);

		for (i= 0; i < sc->ncol; i++) {
			/* Check for color changes. */
			if (vln->fcol[i] != pln->fcol[i] ||
			    vln->bcol[i] != pln->bcol[i]) {
				pln->fcol[i]= vln->fcol[i];
				pln->bcol[i]= vln->bcol[i];
				force= 1;
			}

			if (force || vln->text[i] != pln->text[i]) {
				e_term_fgcol(tr, pln->fcol[i]);
				e_term_bgcol(tr, pln->bcol[i]);
				e_term_move(e, i);
				e_term_putc(vln->text[i]);
				pln->text[i]= vln->text[i];
			}
		}
	}

	/* restore the original position of the cursor. */
	e_term_move(sc->row, sc->col);
	e_term_flush();
}
