/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_H
#define _ECO_H

typedef struct _E_Eco {
	/* tell when need leave the loop.. */
	int loop;

	/* terminal struct. */
	E_Term *tr;

	/* virtual terminal. */
	E_Screen *sc;

	/* buffer list. */
	E_Buffer *buffers;

	/* active view and buffer. */
	E_View *view;

	/* view list. */
	E_View *view_list;

	/* default screen colors. */
	int fg;
	int bg;

	/* Last character (only on "text-mode"). */
	int last_ch;
} E_Eco;

E_Buffer *e_main_find(E_Eco *ec, char *name);
void e_main_rotate(E_Eco *ec);
void e_main_add(E_Eco *ec, E_Buffer *bf);
void e_main_remove(E_Eco *ec, E_Buffer *bf);

void e_main_view_add(E_Eco *ec, E_View *v);
void e_main_view_rem(E_Eco *ec, E_View *v);
E_View *e_main_view_find(E_Eco *ec, E_Buffer *b);

#endif /* _ECO_H */
