/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"
#include "status.h"


void e_status_putc(E_Eco *ec, int c)
{
	if (ec->view->stcol > ec->view->rcol)
		return;

	e_term_move(ec->view->strow, ec->view->stcol);
	if (c == '*') {
		e_term_fgcol(ec->tr, E_TR_RED);
		e_term_bgcol(ec->tr, E_TR_BLACK);
	}
	else {
		e_term_fgcol(ec->tr, ec->view->stfg);
		e_term_bgcol(ec->tr, ec->view->stbg);
	}

	e_term_putc(c);
	ec->view->stcol++;
}

void e_status_putc_string(E_Eco *ec, char *s)
{
	int i;

	for (i= 0; i < strlen (s); i++)
		e_status_putc(ec, s[i]);
}

void e_status_putc_msg(E_Eco *ec)
{
	int i;

	if (!ec->view->stmsg)
		return;

	for (i= 0; i < ec->view->stused; i++)
		e_status_putc(ec, ec->view->stbuf[i]);
}

void e_status_set_msg(E_Eco *ec, char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vsnprintf(ec->view->stbuf, 79, fmt, ap);
	ec->view->stused= strlen(ec->view->stbuf);
	va_end(ap);

	ec->view->stused++;
	ec->view->stmsg= 1;
	return;
}

char *e_status_get_msg(E_Eco *ec, char *prompt)
{
	int i, c, val;

	ec->view->stcol= ec->view->col;
	ec->view->strow= ec->view->rrow;

	/* need clean the status line. */
	/* this work in all terms ? */
	e_term_move(ec->view->strow, ec->view->stcol);
	e_term_eeol();
	i= 0;
	val= 0;

	if (prompt)
		e_status_putc_string(ec, prompt);
	else
		e_status_putc(ec, ':');

	for (;;) {
		c= e_term_getc();
		if (c == '\r') {
			ec->view->stbuf[i]= '\0';
			if (i)
				val= 1;
			break;
		}
		else if ((c == '\b' || c == 0x7F) && i) {
			ec->view->stcol--;
			e_status_putc(ec, ' ');
			ec->view->stcol--;
			e_term_move(ec->view->strow, ec->view->stcol);
			ec->view->stbuf[i]= '\0';
			i--;
			e_term_flush();
		}
		else if (c > 0x00 && c < 0x20) {
			c= c + '@';
			if (c == 'G')
				return(NULL);
		}
		else if ((c >= 0x20) && (c < 0x7f)) {
			ec->view->stbuf[i]= c;
			i++;
			e_status_putc(ec, c);
			e_term_flush();
		}
	}

	if (val)
		return(ec->view->stbuf);
	return(NULL);
}

void e_status_draw(E_Eco *ec)
{
	char buf[80];
	int ocol, orow;
	int rcol, ccol, i, len;

	/*
	 * The status draw is always call after e_update, so
	 * the physical and virtual cursor are already sync,
	 * we need save the current position to restore
	 * in the end.
	 */
	orow= ec->tr->row;
	ocol= ec->tr->col;

	ec->view->stcol= ec->view->col;
	ec->view->strow= ec->view->rrow;

	if (ec->view->b->flag & BUFFER_FLUSH)
		e_status_putc(ec, '*');

	e_status_putc(ec, '(');
	e_status_putc_string(ec, ec->view->b->name);

	for (i= 0, rcol= 0; i < ec->view->b->dot; i++) {
		if (ec->view->b->line->text[i] == '\t')
			rcol+= 7;
		rcol++;
	}

	for (i= 0, ccol= 0; i < ec->view->b->line->used; i++) {
		if (ec->view->b->line->text[i] == '\t')
			ccol+= 7;
		ccol++;
	}

	len= snprintf(buf, 79, ") L(%d/%d) C(%d/%d) ", ec->view->b->nl, ec->view->b->nlines,
			rcol+1, ccol+1);
	if (len == 79 || len == -1)
		buf[79]= '\0';

	e_status_putc_string(ec, buf);

	/* put the percent of the file. */
	if (ec->view->b->line == ec->view->b->lines)
		e_status_putc_string(ec, "Top ");
	else if (!ec->view->b->line->next)
		e_status_putc_string(ec, "Bottom ");
	else {
		int ratio= (100L * ec->view->b->nl)/ec->view->b->nlines;

		if (ratio > 99)
			ratio= 99;

		sprintf(buf, "%2d%% ", ratio);
		e_status_putc_string(ec, buf);
	}

	e_status_putc_msg(ec);
	while (ec->view->stcol < ec->view->rcol)
		e_status_putc(ec, ' ');

	e_term_move(orow, ocol);
	e_term_flush();
}
