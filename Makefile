#
# Makefile for Eco.
#

#
# Custom options.
BUILD =

#
# Install directory.
PREFIX = $(HOME)
BINDIR = $(PREFIX)/bin

#
# User OPTIONS
#

# Add the following line to get an eco.debug file
#BUILD += -DWITH_DEBUG

CC = gcc
CFLAGS = -O2 -Wall -g -funsigned-char -Wdeclaration-after-statement -Werror
LIBS = -lcurses

#
# End of user options
#

OBJS = term.o
OBJS += screen.o
OBJS += status.o
OBJS += key.o
OBJS += main.o
OBJS += buffer.o
OBJS += update.o
OBJS += file.o
OBJS += cmd.o
OBJS += debug.o
OBJS += func.o
OBJS += config.o
OBJS += kill.o
OBJS += view.o
OBJS += time.o
OBJS += syntax.o
OBJS += list.o

TARGET = eco

.PHONY: clean install uninstall

all: $(TARGET)

$(TARGET) : $(OBJS)
	$(CC) $(OBJS) -o $(TARGET) $(LIBS) $(CFLAGS) $(BUILD)

%.o : %.c
	$(CC) -c $(CFLAGS) $(BUILD) $< -o $@

clean:
	rm -f $(OBJS) $(TARGET)

install:
	cp $(TARGET) $(BINDIR)

uninstall:
	rm -f $(BINDIR)/$(TARGET)
