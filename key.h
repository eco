/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_KEY_H
#define _ECO_KEY_H

typedef struct _E_Key {
	struct _E_Key *next;

	/* need ctrl + X ? */
	short ctrl_x;

	/* key code. */
	short code;

	/* callback. */
	void (*exec)(E_Eco *);
} E_Key;

void e_key_free(void);

void e_key_add(short code, short ctrl_x, void (*cb)(E_Eco *));
void e_key_remove(short code, short ctrl_x);
int e_key_exec(E_Eco *ec, short code, short ctrl_x);

#endif /* _ECO_KEY_H */
