/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "term.h"
#include "screen.h"
#include "buffer.h"
#include "view.h"
#include "eco.h"


/* "kill" buffer, all the text that you cut/remove go here. */
E_Buffer *kill_buf= NULL;

void e_kill_init(void)
{
	kill_buf= e_buffer_new("KillBuffer");
	kill_buf->flag |= BUFFER_NOIDENT;
}

void e_kill_end(void)
{
	e_buffer_free(kill_buf);
}

void e_kill_cut(E_Line *ln)
{
	int i;

	if (!ln) {
		/* only a new line. */
		e_buffer_newline(kill_buf);
		return;
	}

	if (!ln->used) {
		/* just a \n. */
		e_buffer_newline(kill_buf);
		return;
	}

	for (i= 0; i < ln->used; i++)
		e_buffer_insert(kill_buf, ln->text[i], 1);
}

void e_kill_paste(E_Buffer *bf)
{
	E_Line *p;

	/* avoid identation. */
	bf->flag |= BUFFER_NOIDENT;

	p= kill_buf->lines;
	while (p) {
		e_buffer_insert_str(bf, p->text, p->used);
		e_buffer_newline(bf);
		p= p->next;
	}

	/* Remove the flag. */
	bf->flag &= ~BUFFER_NOIDENT;
}

void e_kill_clean(void)
{
	/* just drop the buffer, this is bad, but work ;) */
	e_kill_end();
	e_kill_init();
}
