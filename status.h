/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_STATUS_H
#define _ECO_STATUS_H

void e_status_draw(E_Eco *ec);
void e_status_set_msg(E_Eco *ec, char *fmt, ...);
char *e_status_get_msg(E_Eco *ec, char *prompt);

#endif /* _ECO_STATUS_H */
