/*
 * Copyright (C) 2008 Diego Hernan Borghetti.
 * Eco
 */

#ifndef _ECO_VIEW_H
#define _ECO_VIEW_H

typedef struct _E_View {
	struct _E_View *next;
	struct _E_View *prev;

	/* The "View" is a region of the screen when we
	 * draw the buffer, it's like a canvas.
	 *
	 * The screen can be split in N number of view
	 * and every view have a buffer attached.
	 *
	 *
	 * The row and col have the start position of the
	 * view, in screen coordinates.
	 *
	 * The nrow and ncol is the size of the view, but
	 * _NOT_ in the screen.
	 *
	 * The rrow and rcol is the end position of the
	 * view, in screen coordinates (srow + row, scol + col)
	 */

	/* start position. */
	int row;
	int col;

	/* view size. */
	int nrow;
	int ncol;

	/* real view size. */
	int rrow;
	int rcol;

	/* buffer attached to this view. */
	E_Buffer *b;

	/* last search string. */
	char search[80];

	/* status line. */
	int stfg;
	int stbg;

	/* minibuf for status line. */
	char stbuf[80];

	/* minibuf position. */
	int stcol;
	int strow;
	int stused;

	/* have a message to show ? */
	int stmsg;

	/* view flag. */
	char flag;
} E_View;

/* view->flag */
#define VIEW_REDRAW	(1<<0)
#define VIEW_SHOW	(1<<1)

E_View *e_view_new(E_Screen *sc);
void e_view_free(E_View *v);

void e_view_move(E_View *v, int row, int col);
void e_view_resize(E_View *v, int nrow, int ncol);

void e_view_show(E_View *v);
void e_view_unshow(E_View *v);
void e_view_redraw(E_View *v);

#endif /* _ECO_VIEW_H */
